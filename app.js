const express = require('express');
const app = express();
const port = 3000;
const {json} = require('express');
const fs = require('fs');
const user = require('./data/user.json')
var expressLayouts = require('express-ejs-layouts');
const bodyParser = require('body-parser');
// const fs = require('fs');



app.use(bodyParser.urlencoded());
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.use(express.static('public'))
app.use(expressLayouts);



app.get('/', (req, res) => {
    res.render('home',{
        layout:'layouts/main-layouts'
    })
})


app.get('/challenge3', (req, res) => {
    res.sendFile('views/challenge3.html', {root:__dirname})
})

app.get('/challenge4', (req, res) => {
    res.sendFile('views/challenge4.html', {root:__dirname})
})


app.get('/ID&Pass', (req, res) => {
    const users = loadId()
    res.render('ID&Pass',{
        layout:'layouts/main-layouts',
        users,
    })
})

app.get('/user', (req, res) => {
    res.status(200).json(user)
})



app.get('/challenge3/login', (req, res) => {
    res.render('login',{
        layout:'layouts/main-layouts',
    })
})

app.post('/challenge3/login', (req, res) => {
    const {Id, Pass} = req.body;
    const isUserExist = user.filter(i => i.Id === Id);
    let isPasswordCorrect = false;
    if(isUserExist.length > 0){
        if(Pass === isUserExist[0].Pass){
            isPasswordCorrect = true
        }
    }
    if(isPasswordCorrect){
        res.status(200).send('anda berhasil login')
    }
    res.render('alert')
})

const loadId = () => {
    const file = fs.readFileSync('data/user.json')
    const users = JSON.parse(file)
    return users
}

app.listen(port, () => console.log(`port ${port} sudah berjalan dengan baik`));